import { Component, OnInit } from '@angular/core';
import { DataService } from '../_services/data.service';

@Component({
  selector: 'app-alert-grouplist',
  templateUrl: './alert-grouplist.component.html',
  styleUrls: ['./alert-grouplist.component.scss']
})
export class AlertGrouplistComponent implements OnInit {
  alerts:any;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getAlertDetails().subscribe((option: Array<object>) => {
      this.alerts=option;
     console.log(this.alerts)
    });
  }

}
