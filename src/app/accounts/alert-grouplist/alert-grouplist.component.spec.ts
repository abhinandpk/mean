import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertGrouplistComponent } from './alert-grouplist.component';

describe('AlertGrouplistComponent', () => {
  let component: AlertGrouplistComponent;
  let fixture: ComponentFixture<AlertGrouplistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertGrouplistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertGrouplistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
