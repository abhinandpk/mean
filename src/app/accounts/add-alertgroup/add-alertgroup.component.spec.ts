import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAlertgroupComponent } from './add-alertgroup.component';

describe('AddAlertgroupComponent', () => {
  let component: AddAlertgroupComponent;
  let fixture: ComponentFixture<AddAlertgroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAlertgroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAlertgroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
