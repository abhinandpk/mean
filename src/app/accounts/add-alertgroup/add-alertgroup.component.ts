import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertGroup, Rule, Channel } from '../shared/models/alertGroup.model';
import { DataService } from '../_services/data.service';
@Component({
  selector: 'app-add-alertgroup',
  templateUrl: './add-alertgroup.component.html',
  styleUrls: ['./add-alertgroup.component.scss']
})
export class AddAlertgroupComponent implements OnInit {
  myform: FormGroup;
  alertGroup: AlertGroup = new AlertGroup();
  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private dataService: DataService) { }

  ngOnInit() {
    this.initForm(this.alertGroup);
    // this.myform = this._formBuilder.group({
    //   groupId: [''],
    //   rules: this._formBuilder.array([this._formBuilder.group({
    //     type: [''],
    //     min: [''],
    //     max: [''],
    //     channels: this._formBuilder.group({
    //       sms: [false],
    //       email: [false],
    //       pushNotification: [false]
    //     })
    //   })
    //   ]),

    // });
  }
  initForm(alert: AlertGroup) {
    this.myform = this._formBuilder.group({
      groupId: [alert.groupId],
      rules: this._formBuilder.array(alert.rules.length === 0 ? [] : alert.rules.map(rule => this.initRules(rule))),
      channels: this._formBuilder.array(alert.channels.length === 0 ? []: alert.channels.map(channel => this.initChannels(channel)))
    });
  }
  initRules(alertRule: Rule) {
    return this._formBuilder.group({
      type: [alertRule.type],
      minValue: [alertRule.minValue],
      maxValue: [alertRule.maxValue]
    })
  }
  initChannels(channel: Channel){
    return this._formBuilder.group({
      sms:[channel.sms],
      email:[channel.sms],
      pushNotification: [channel.sms]
    })
  }
  get alert(): FormArray {
    return this.myform.get('rules') as FormArray;
  }
  addAlert(): void {
    this.alert.push(this._formBuilder.group({ type: '', minValue: '', maxValue: ''}));
  }
  get channel(): FormArray {
    return this.myform.get('channels') as FormArray;
  }
  deleteAlert(index): void {
    this.alert.removeAt(index);
  }
  addNewAlert() {
    console.log(this.myform.value)
    const newAlert = Object.assign(this.alertGroup, this.myform.value);
    console.log(newAlert);
    // this.dataService.addNewAlertGroup(newAlert).subscribe(res => {
    //   console.log(res);
    //   this.router.navigate(['/alert-list']);
    // });

  }
}
