import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { AccountsRoutingModule } from './accounts-routing.module';
import { AccountsComponent } from './accounts.component';
import { AddAccountComponent } from './add-account/add-account.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AddAlertgroupComponent } from './add-alertgroup/add-alertgroup.component';
import { AlertGrouplistComponent } from './alert-grouplist/alert-grouplist.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { TransactionGeneratorComponent } from './transaction-generator/transaction-generator.component';
import {  HttpClientModule } from '@angular/common/http';
import { DataService } from './_services/data.service';
import { AddNewCustomerComponent } from './add-new-customer/add-new-customer.component';
import { AccountDashboardComponent } from './account-dashboard/account-dashboard.component';
import { FusionChartsModule } from 'angular-fusioncharts';
// Import FusionCharts library
import * as FusionCharts from 'fusioncharts';

// Load FusionCharts Individual Charts
import * as Charts from 'fusioncharts/fusioncharts.charts';
import { PieChartComponent } from './account-dashboard/pie-chart/pie-chart.component';

FusionChartsModule.fcRoot(FusionCharts, Charts)
@NgModule({
  declarations: [AccountsComponent, AddAccountComponent, AddCustomerComponent, AccountListComponent, AddAlertgroupComponent, AlertGrouplistComponent, CustomerListComponent, TransactionGeneratorComponent, AddNewCustomerComponent, AccountDashboardComponent, PieChartComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TypeaheadModule,
    AccountsRoutingModule,
    FusionChartsModule
  ],
  providers: [DataService]
})
export class AccountsModule { }
