import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddAccountComponent } from './add-account/add-account.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AddAlertgroupComponent } from './add-alertgroup/add-alertgroup.component';
import { AlertGrouplistComponent } from './alert-grouplist/alert-grouplist.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { TransactionGeneratorComponent } from './transaction-generator/transaction-generator.component';
import { AddNewCustomerComponent } from './add-new-customer/add-new-customer.component';
import { AccountDashboardComponent } from './account-dashboard/account-dashboard.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'account-dashboard',
  },
  {
    path: 'account-balance/:id',
    component: AddAccountComponent,
  },
  {
    path: 'add-customer',
    component: AddCustomerComponent
  },
  {
    path: 'account-list',
    component: AccountListComponent
  },
  {
    path: 'add-alert',
    component: AddAlertgroupComponent
  },  {
    path: 'alert-list',
    component: AlertGrouplistComponent
  },
  {
    path: 'customer-list',
    component: CustomerListComponent
  },
  {
    path: 'transaction-generator',
    component: TransactionGeneratorComponent
  },
  {
    path: 'addnew-customer',
    component: AddNewCustomerComponent
  },
  {
    path: 'account-dashboard',
    component: AccountDashboardComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
