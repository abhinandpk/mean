import { Component, OnInit } from '@angular/core';
import {DataService} from '../_services/data.service';
import { Customer } from '../shared/models/customer.model';
@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  customers:any;
  constructor( private dataService: DataService) { }

  ngOnInit() {
    this.getCustomers();
  }
  getCustomers(){
    // this.dataService.getCustomer().subscribe(res=>{
    //  this.customers = res;
    // })
    this.dataService.getAllCustomers().subscribe(res => {
      this.customers = res
      console.log(res);
    })
  }
}
