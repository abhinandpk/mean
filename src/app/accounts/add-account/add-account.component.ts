import { Component, OnInit} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router'
import { DataService } from '../_services/data.service';
import { Type } from '../utility/type';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.scss']
})

export class AddAccountComponent implements OnInit {
  accountForm: FormGroup;
  selected: string;
  alerts:any ;
  alert_Data:any = {};
  tableData:boolean = false;
  account:any;
  editData:any ={};

  formData:boolean = false;
  registerid:any;

  enableCustomerAdd: Boolean = false;

  public numberCustomers: number[];
  constructor(private dataService: DataService,private router:Router,private act_route: ActivatedRoute,private _formBuilder: FormBuilder) {
    this.numberCustomers = new Array(10);
  
  }

  ngOnInit() {
    this.dataService.getAlertDetails().subscribe((option: Array<object>) => {
      this.alerts=option;
    });
    this.dataService.currentMessage.subscribe(message => this.account= message)
   

    this.act_route.params.subscribe(params => {
      this.registerid = params['id'];
    });
   if(this.registerid == Type.editAccount ){
   this.editData= this.account;
    }
    this.accountForm = this._formBuilder.group({
      accountNumber:['',Validators.required],
      accountType:['',Validators.required],
      customerno: ['', Validators.required],
      alertGroup: [''],
      alertStatus:[''],
      actualBalance:['',Validators.required],
      availableBalance:['',Validators.required]
    })

  }
  typeaheadOnSelect(alert){
    console.log(alert);
    this.tableData = true;
    this.alert_Data = alert.item;
    //this.accountForm.value.alertGroup = 
  }

  
  accountDetails={
    'customerno':'',

  }
  addAccount(){
    this.enableCustomerAdd = true;
    this.accountDetails={
      'customerno' : this.accountForm.value.customerno
    }  
    this.dataService.newMessage(this.accountDetails);
}
}
