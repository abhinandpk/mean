import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../_services/data.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Customer } from '../shared/models/customer.model';
@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {
  @Input() accountInfo: any
  selected: string;
  customer: Array<any>;
  customer_Data: any = {}
  accountDetails: any;
  addCustomerForm: FormGroup;
  search: FormControl = new FormControl();
  customerCount = 0;
  customerIds: Array<String> = [];
  currentCustomer: Customer = new Customer();
  constructor(private dataService: DataService,
    private _formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.dataService.getCustomer().subscribe((option: Array<object>) => {
      this.customer = option;
    });
    this.dataService.newcurrentMessage.subscribe(newmessage => this.accountDetails = newmessage);
    this.initForm(this.currentCustomer);
  }
  typeaheadOnSelect(event) {
    this.customer_Data = event.item;
    this.initForm(event.item);
  }
  initForm(customer: Customer) {
    this.addCustomerForm = this._formBuilder.group({

      id: [{ value: customer.id, disabled: true }],
      name: [{ value: customer.name, disabled: true }],
      emailId: [{ value: customer.emailId, disabled: true }],
      phoneNumber: [{ value: customer.phoneNumber, disabled: true }],
      alertStatus: [{ value: customer.alertStatus, disabled: true }]
    });
  }
  addCustomer() {
    this.customerCount++;
    this.customerIds.push(this.addCustomerForm.value.id);
    this.customer.map((customer,i) => {
        if(customer.id == this.addCustomerForm.value.id){
            this.customer.splice(i,1);
        }
      });
    this.initForm(this.currentCustomer);
    this.search.setValue('');
    if (this.customerCount == this.accountInfo.customerno) {
      this.customerCount = 0;
      this.accountInfo.customerIds = this.customerIds;
      delete this.accountInfo.customerno;
      // this.dataService.addAccount(this.accountInfo).subscribe(res =>{
      //   console.log(res);
      // })
    }
  }
}
