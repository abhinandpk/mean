import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router'
import { DataService } from '../_services/data.service';
import { Type } from '../utility/type'

@Component({
  selector: 'app-account-list',

  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {
  accounts:any;

  constructor(private router:Router,private dataService: DataService,private act_route: ActivatedRoute) { 
  
  }

  ngOnInit() {
    this.dataService.getAccountDetails().subscribe((option: Array<object>) => {
      this.accounts=option;
    });
  }
newAccount(){
this.router.navigate(['//account-balance/:j']);
}
editAccount(account){
  this.dataService.changeMessage(account);
  this.router.navigate(['/account-balance/'+ Type.editAccount]);
}
}
