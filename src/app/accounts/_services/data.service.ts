import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { GlobalUrl } from '../utility/GlobalUrl';
import { Customer } from '../shared/models/customer.model';
import { AccountModel } from '../shared/models/account.model';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  message = {}
  newmessage = {}
  private messageSource = new BehaviorSubject(this.message);
  private newmessageSource = new BehaviorSubject(this.newmessage);
  currentMessage = this.messageSource.asObservable();
  newcurrentMessage = this.newmessageSource.asObservable();
  constructor(private httpClient: HttpClient,
    private GlobalUrl: GlobalUrl) { }

  changeMessage(message: any) {
    this.messageSource.next(message)
  }
  newMessage(newmessage: any) {
    this.newmessageSource.next(newmessage)
  }
  getAlert() {
    return this.httpClient.get('assets/mock_data/alertgroup.json');
  }
  getAccountDetails() {
    return this.httpClient.get('assets/mock_data/account_test.json');
  }
  getAlertDetails() {
    return this.httpClient.get('assets/mock_data/alert_test.json');
  }
  getCustomer() {
    return this.httpClient.get('assets/mock_data/customers-test.json');
  }
  getAllCustomers() {
    return this.httpClient.get(`${this.GlobalUrl.API_BASE_URL}getAllCustomers`);
  }
  addNewCustomer(customerDetails: Customer) {
    return this.httpClient.post(`${this.GlobalUrl.API_BASE_URL}addCustomers`, customerDetails);
  }
  addNewAlertGroup(alertGroup) {
    return this.httpClient.post(`${this.GlobalUrl.API_BASE_URL}addAlertGroup`, alertGroup);
  }
  addAccount(accountDetails: AccountModel) {
    return this.httpClient.post(`${this.GlobalUrl.API_BASE_URL}addAccount`, accountDetails)
  }
}
