export class AlertGroup{
    groupId: string;
    rules: Array<Rule>;
    channels: Array<Channel>;
    constructor(){
        this.rules = [{
            type:'',
            minValue:'',
            maxValue: '',
        }];
        this.channels = [{
            sms: false,
            email: false,
            pushNotification: false
        }]
    }
}

export class Rule{
    type:string;
    minValue: string;
    maxValue: string;
}
export class Channel{
    sms: boolean;
    email: boolean;
    pushNotification: boolean
}