export class AccountModel {
    accountNumber: string;
    accountType: string;
    customerIds: Array<string>;
    alertGroup: string;
    alertStatus: Number;
    actualBalance: string;
    availableBalance: string;
}