export class Customer{
    id: string;
    name: string;
    emailId: string;
    phoneNumber: string;
    alertStatus: string;
}