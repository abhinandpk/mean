import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DataService } from '../_services/data.service';
import { Router } from '@angular/router';
import { Customer } from '../shared/models/customer.model';
@Component({
  selector: 'app-add-new-customer',
  templateUrl: './add-new-customer.component.html',
  styleUrls: ['./add-new-customer.component.scss']
})
export class AddNewCustomerComponent implements OnInit {
  addCustomerForm: FormGroup;
  customer: Customer = new Customer();
  alertStatus = [{ status: "Enabled", value: 1 }, { status: "Disabled", value: 0 }]
  constructor(private _formBuilder: FormBuilder,
    private dataService: DataService,
    private router: Router) { }

  ngOnInit() {
    this.initForm(this.customer);
  }
  initForm(customer: Customer) {
    this.addCustomerForm = this._formBuilder.group({
      id: [customer.id, Validators.required],
      name: [customer.name, Validators.required],
      emailId: [customer.emailId, Validators.email],
      phoneNumber: [customer.phoneNumber],
      alertStatus: [customer.alertStatus]
    });
  }
  addCustomer() {
    const customerDetails = Object.assign(this.customer, this.addCustomerForm.value);
    customerDetails.alertStatus = Number(customerDetails.alertStatus);
    this.dataService.addNewCustomer(customerDetails).subscribe(res => {
      console.log(res);
      this.router.navigate(['/customer']);
    })
  }
}


